+ Gör så att Raspberry Pi automatiskt hämtar hem förändringar i git-repot och ser till uppdateringarna visas
+ Gör så att elevhälsan själva kan redigera textbaserad information
+ Möjliggör för elevhälsan att ta bort/lägga till slides/personer
+ Gör så att elevhälsan själva kan redigera profilbild
+ Hårdkoda in "info-ruta" med ett tips/information för respektive person
+ Gör så att elevhälsan själva kan redigera innehållet i info-rutan
